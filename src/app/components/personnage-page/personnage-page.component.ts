import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms'
import { PersonnageModel } from 'src/app/model/personnage';
import { PersonnageService } from 'src/app/services/personnage.service';

@Component({
  selector: 'app-personnage-page',
  templateUrl: './personnage-page.component.html',
  styleUrls: ['./personnage-page.component.css']
})
export class PersonnagePageComponent implements OnInit {

  formValue !: FormGroup;
  personnageObj : PersonnageModel = new PersonnageModel();
  personnageData !: any;
  showAdd !: boolean;
  showUpdate !: boolean;
  constructor(private formbuilder: FormBuilder, private persoService: PersonnageService) { }

  ngOnInit(): void {
    this.formValue = this.formbuilder.group({
      title : [''],
      key : [''],
      name : [''],
      active: ['']
    })
    this.getAllPersonnage();
  }
  clickAddPersonnage = () => {
    this.formValue.reset();
    this.showAdd = true;
    this.showUpdate = false;
  }
  postPersonnageDetails = () => {
    this.personnageObj.title = this.formValue.value.title;
    this.personnageObj.key   = this.formValue.value.key;
    this.personnageObj.name  = this.formValue.value.name;
    this.personnageObj.active= this.formValue.value.active;

    this.persoService.postPersonnage(this.personnageObj)
    .subscribe(res=>{
      console.log(res);
      alert("personnage ajouter avec succés")
      let ref = document.getElementById('cancel') // pour fermer le formulaire automatiquement
      ref?.click(); // pour fermer le formulaire automatiquement
      this.formValue.reset();
      this.getAllPersonnage();
    },err=>{
      alert("une erreur c'est produit")
    })
  }

  getAllPersonnage = () => {
    this.persoService.getPersonnage()
    .subscribe(
      res=>{
        this.personnageData = res;
      }
    )
  }

  deletePerso = (row: any) => {
    this.persoService.deletePersonnage(row.id)
    .subscribe(
      res=>{
        alert("Personnage Supprimer");
        this.getAllPersonnage();
      }
    )
  }

  editPerso = (row: any) => {
    this.showAdd = false;
    this.showUpdate = true;
    this.personnageObj.id = row.id;
    this.formValue.controls['title'].setValue(row.title);
    this.formValue.controls['key'].setValue(row.key);
    this.formValue.controls['name'].setValue(row.name);
    this.formValue.controls['active'].setValue(row.active);
  }
  updatePersonnageDetails = () => {
    this.personnageObj.title = this.formValue.value.title;
    this.personnageObj.key   = this.formValue.value.key;
    this.personnageObj.name  = this.formValue.value.name;
    this.personnageObj.active= this.formValue.value.active;

    this.persoService.updatePersonnage(this.personnageObj, this.personnageObj.id)
    .subscribe(
      res=>{
        alert("Personnage Modifier avec succés");
        let ref = document.getElementById('cancel') // pour fermer le formulaire automatiquement
        ref?.click(); // pour fermer le formulaire automatiquement
        this.formValue.reset();
        this.getAllPersonnage();
      }
    )
  }
}
