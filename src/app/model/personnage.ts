export class PersonnageModel{
    id: number = 0;
    title: string = '';
    key: string = '';
    name: string = '';
    active: boolean = false;
}